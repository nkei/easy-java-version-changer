# Easy java version changer

Utilutility for easy and quick switching between installed on machine JAVA versions. .NET Framework 3.5 is required to work, an installation request will appear at the first launch.

![image](screen.png)

Download (v.1.0.0): [easy_java_changer.exe](https://gitlab.com/nkei/easy-java-version-changer/-/raw/master/easy_java_changer.exe?inline=false)

Result antivirus scan [link](https://online396.drweb.com/cache/?i=83d95b902463a983333fc68f571f7b2f)

![image](antivirus-result.png)

