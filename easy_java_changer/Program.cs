using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text.RegularExpressions;

namespace easy_java_changer
{
    class Program
    {
        static void Main(string[] args)
        {   
            Console.WriteLine("============== EASY JAVA VERSION CHANGER ==============");
            Console.WriteLine();
            Console.Write("Run from Administrator... ");
            if (!runFromAdministrator())
            {
                return;
            };
            Console.WriteLine("run from Administrator success");

            printCurrentVersion();
            var environmentVariableTarget = EnvironmentVariableTarget.Machine;
            string javaHome = Environment.GetEnvironmentVariable("JAVA_HOME",
                        environmentVariableTarget);
            if (javaHome == null)
            {
                Console.WriteLine("Machine variable not found... try get User variable");
                environmentVariableTarget = EnvironmentVariableTarget.User;
                javaHome = Environment.GetEnvironmentVariable("JAVA_HOME",
                    environmentVariableTarget);     
            }
            
            string path = Environment.GetEnvironmentVariable("PATH",
                environmentVariableTarget);
            
            Console.WriteLine("JAVA_HOME: " + javaHome);
            Console.WriteLine();

            var j = javaHome.Split('\\');
            j = j.Take(j.Length - 1).ToArray();            
            int i = 0;
            var javaInstalls = Directory.GetDirectories(String.Join("\\", j)).ToDictionary(k => i++);            
            foreach (var p in javaInstalls)
            {
                Console.WriteLine(p);
            }
            Console.WriteLine();

            string pattern = "[0-" + (javaInstalls.Count() - 1) + "]";            
            bool validVersionKey = false;
            while (!validVersionKey)
            {
                Console.Write("Select new JAVA version " + pattern + ": ");
                var newVersionKey = Console.ReadLine();                

                if (Regex.IsMatch(newVersionKey, pattern + "+"))  {
                    validVersionKey = true;
                    int newVersionKeyInt = int.Parse(newVersionKey);
                    Console.WriteLine("Selected version = " + javaInstalls.ToList()[newVersionKeyInt]);
                    string newJavaHome = javaInstalls[newVersionKeyInt];
                    Console.Write("Update JAVA_HOME and PATH... ");                    
                    Environment.SetEnvironmentVariable("JAVA_HOME", newJavaHome, 
                        environmentVariableTarget);
                    Environment.SetEnvironmentVariable("PATH", path.Contains(javaHome) ? path.Replace(javaHome, newJavaHome) : newJavaHome + "\\bin;" + path, 
                        environmentVariableTarget);
                    Console.WriteLine("environments updated.");
                    Console.WriteLine();
                } else
                {
                    Console.WriteLine("Wrong index");
                }              
            }
            Console.WriteLine();
            javaHome = Environment.GetEnvironmentVariable("JAVA_HOME",
                        environmentVariableTarget);            
            Console.WriteLine("JAVA_HOME: " + javaHome);            
            Console.ReadKey();
        }

        private static void printCurrentVersion()
        {
            Console.WriteLine("============== JAVA CURRENT VERSION ==============");
            Console.WriteLine();
            var java = new System.Diagnostics.Process();
            java.StartInfo.FileName = "java.exe";            
            java.StartInfo.Arguments = "-version";
            java.StartInfo.UseShellExecute = false;
            java.StartInfo.RedirectStandardInput = true;            
            java.Start();
            java.WaitForExit();
            Console.WriteLine();
            Console.WriteLine("==================================================");
            Console.WriteLine();
        }

        private static bool runFromAdministrator()
        {
            WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
            if (!hasAdministrativeRight)
            {
                string fileName = Assembly.GetExecutingAssembly().Location;
                ProcessStartInfo processInfo = new ProcessStartInfo();
                processInfo.Verb = "runas";
                processInfo.FileName = fileName;
                
                try
                {
                    Process.Start(processInfo);                    
                }
                catch (Win32Exception)
                {
                    Console.WriteLine("I can't change JAVA version without administrator rights... EXIT");
                    return false;
                }                
            }
            return hasAdministrativeRight;
        }
    }
}
